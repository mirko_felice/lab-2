package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import Optionals.Option._

class OptionalTest {

  @Test def testFilter() {
    assertEquals(None(), filter(Some(5))(_ > 8))
    assertEquals(Some(5), filter(Some(5))(_ > 2))
  }

  @Test def testMap() {
    assertEquals(Some(true), map(Some(5))(_ > 2))
    assertEquals(None(), map(None[Int]())(_ > 2))
  }

  @Test def testMap2() {
    assertEquals(None(), map2(Some(5))(None[Int]())(_ + _ > 2))
    assertEquals(Some(true), map2(Some(5))(Some(3))(_ + _ > 2))
    assertEquals(None(), map2(None[Int]())(Some(3))(_ - _ < 3))
  }
}
