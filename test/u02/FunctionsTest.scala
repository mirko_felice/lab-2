package u02

import org.junit.jupiter.api.Assertions.{assertArrayEquals, assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test
import u02.Functions.{compose, fib, negAsLambda, negAsMethod, p1, p2, p3, p4, parityAsLambda, parityAsMethod}

class FunctionsTest {

  @Test def testParityAsMethod() {
    assertEquals("even", parityAsMethod(2))
    assertEquals("odd", parityAsMethod(55))
  }

  @Test def testParityAsLambda() {
    assertEquals("even", parityAsLambda(2))
    assertEquals("odd", parityAsLambda(55))
  }

  @Test def testNegAsMethod() {
    val empty: String => Boolean = _ == ""
    val notEmpty = negAsMethod(empty)
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

  @Test def testNegAsLambda() {
    val empty: String => Boolean = _ == ""
    val notEmpty = negAsLambda(empty)
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

  @Test def testNegAsMethodGeneric() {
    val predicate: Int => Boolean = _ == 1
    val notPredicate = negAsMethod(predicate)
    assertTrue(notPredicate(2))
    assertFalse(notPredicate(1))
    assertTrue(notPredicate(2) && !notPredicate(1))
  }

  @Test def testPredicateCurryingAsLambda() {
    assertTrue(p1(0)(1)(2))
    assertFalse(p1(10)(50)(2))
  }

  @Test def testPredicateNotCurryingAsLambda() {
    assertTrue(p2(0,1,2))
    assertFalse(p2(10,50,2))
  }

  @Test def testPredicateCurryingAsMethod() {
    assertTrue(p3(0)(1)(2))
    assertFalse(p3(10)(50)(2))
  }

  @Test def testPredicateNotCurryingAsMethod() {
    assertTrue(p4(0,1,2))
    assertFalse(p4(10,50,2))
  }

  @Test def testCompose() {
    val f: Int => Int = _-1
    val g: Int => Int = _*2
    assertEquals(9, compose(f, g)(5))
    // CONSTRAINT: Dobbiamo indicare per forza il tipo di input e output delle lambda
  }

  @Test def testGenericCompose() {
    val f: Int => Int = _ - 1
    val g: Int => Int = _ * 2
    assertEquals(9, compose(f, g)(5))
    val h: String => String = _ + " test"
    val i: String => String = _ + " prova"
    assertEquals("croce prova test", compose(h, i)("croce"))
    // CONSTRAINT: Se per esempio alla funzione 'i' definiamo input di tipo Int a Runtime darà type mismatch
  }

  @Test def testFibonacci() {
    assertArrayEquals(Array(0,1,1,2,3,55), Array(fib(0), fib(1), fib(2), fib(3), fib(4), fib(10)))
  }
}
