package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u02.Geometry._

class GeometryTest {

  @Test def testRectanglePerimeter() {
    assertEquals(10, perimeter(Rectangle(4,1)))
    assertEquals(20, perimeter(Rectangle(5,5)))
  }

  @Test def testSquarePerimeter() {
    assertEquals(12, perimeter(Square(3)))
    assertEquals(20, perimeter(Square(5)))
  }

  @Test def testCirclePerimeter() {
    assertEquals(2 * Math.PI * 5, perimeter(Circle(5)))
    assertEquals(2 * Math.PI * 3, perimeter(Circle(3)))
  }

  @Test def testRectangleArea() {
    assertEquals(4, area(Rectangle(4,1)))
    assertEquals(30, area(Rectangle(6,5)))
  }

  @Test def testSquareArea() {
    assertEquals(25, area(Square(5)))
    assertEquals(9, area(Square(3)))
  }

  @Test def testCircleArea() {
    assertEquals(Math.PI * 5 * 5, area(Circle(5)))
    assertEquals(Math.PI * 3 * 3, area(Circle(3)))
  }
}
