package u02

object Functions {

  def parityAsMethod(x: Int): String = x match {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  val parityAsLambda: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  def negAsMethod[A](predicate: A => Boolean): A => Boolean = value => !predicate(value)

  val negAsLambda: (String => Boolean) => String => Boolean = f => s => !f(s)

  val p1: Int => Int => Int => Boolean = x => y => z => (x,y,z) match {
    case (x,y,z) if x <= y && y <= z => true
    case _ => false
  }

  val p2: (Int, Int, Int) => Boolean = {
    case (x,y,z) if x <= y && y <= z => true
    case _ => false
  }

  def p3(x:Int) (y:Int) (z:Int): Boolean = (x,y,z) match {
    case (x,y,z) if x <= y && y<= z => true
    case _ => false
  }

  def p4(x:Int, y:Int, z:Int): Boolean = (x,y,z) match {
    case (x,y,z) if x <= y && y<= z => true
    case _ => false
  }

  def compose[A](f: A => A, g: A => A): A => A = x => f(g(x))

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case n => fib(n-1) + fib(n-2)
  }
}
