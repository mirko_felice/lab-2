package u02

object Geometry {

  sealed trait Shape
  case class Rectangle(greaterSide: Double, minorSide: Double) extends Shape
  case class Circle(radius: Double) extends Shape
  case class Square(side: Double) extends Shape

  def perimeter(shape: Shape): Double = shape match {
    case Rectangle(greaterSide, minorSide) => 2 * greaterSide + 2 * minorSide
    case Square(side) => 4 * side
    case Circle(radius) => 2 * Math.PI * radius
  }

  def area(shape: Shape): Double = shape match {
    case Rectangle(greaterSide, minorSide) => greaterSide * minorSide
    case Square(side) => side * side
    case Circle(radius) => Math.PI * radius * radius
  }
}
